import com.devcamp.models.Person;
import com.devcamp.models.Staff;
import com.devcamp.models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Tran Quang Hoang", "VN");
        System.out.println("person 1");
        System.out.println(person1);

        Person person2 = new Person("Tran Thi Cam Tu", "VN");
        System.out.println("person 2");
        System.out.println(person2);

        Student student1 = new Student("Hang", "VN", "ABC", 2023, 100.000);
        System.out.println("student 1");
        System.out.println(student1);

        Student student2 = new Student("Nga", "VN", "FYZ", 2023, 200.000);
        System.out.println("student 2");
        System.out.println(student2);

        Staff staff1 = new Staff("Lam", "USA", "icShool", 1000000000);
        System.out.println("staff 1");
        System.out.println(staff1);

        Staff staff2 = new Staff("Nhu", "CAN", "ThoaiNgocHoc", 2000000000);
        System.out.println("staff 2");
        System.out.println(staff2);
    }
}
